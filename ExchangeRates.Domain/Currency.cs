﻿using System;

namespace ExchangeRates.Domain
{
    public class Currency
    {
        public int Id { get; private set; }

        public string Name { get; set; }
    }
}