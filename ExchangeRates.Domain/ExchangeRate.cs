using System;

namespace ExchangeRates.Domain
{
    public class ExchangeRate
    {
        public int Id { get; private set; }

        public Currency Target { get; set; }

        public int Amount { get; set; }

        public decimal Rate { get; set; }

        public DateTime Date { get; set; }
    }
}