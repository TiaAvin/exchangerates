using Autofac;
using ExchangeRates.Core;
using ExchangeRates.Dal.Repositories;

namespace ExchangeRates.Dal
{
    public class DalModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ExchangeRateRepository>().As<IExchangeRateRepository>();
        }
    }
}