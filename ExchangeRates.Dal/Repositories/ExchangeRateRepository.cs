using System;
using System.Collections.Generic;
using System.Linq;
using ExchangeRates.Data;
using ExchangeRates.Domain;
using Microsoft.EntityFrameworkCore;

namespace ExchangeRates.Dal.Repositories
{
    internal class ExchangeRateRepository : IExchangeRateRepository
    {
        private readonly IDataContext _dataContext;
        
        public ExchangeRateRepository(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IEnumerable<ExchangeRate> GetExchangeRatesOnDates(params DateTime[] dates)
        {
            var dateCollection = dates ?? Enumerable.Empty<DateTime>().ToArray();
            
            var exchangeRates = _dataContext
                .ExchangeRates
                .Include(x => x.Target)
                .Where(x => dateCollection.Contains(x.Date))
                .ToArray();

            return exchangeRates;
        }

        public ExchangeRate GetExchangeRateForCurrencyActualOnDate(int currencyId, DateTime date)
        {
            return _dataContext
                .ExchangeRates
                .Include(x => x.Target)
                .Where(x => x.Target.Id == currencyId)
                .Where(x => x.Date <= date)
                .OrderByDescending(x => x.Date)
                .FirstOrDefault();
        }

        public void SaveRates(IEnumerable<ExchangeRate> exchangeRates)
        {
            var exchangeRateCollection = (exchangeRates ?? Enumerable.Empty<ExchangeRate>()).ToArray();
            
            _dataContext
                .ExchangeRates
                .AddRange(exchangeRateCollection.Where(x => x.Id <= 0));
            
            _dataContext
                .ExchangeRates
                .UpdateRange(exchangeRateCollection.Where(x => x.Id > 0));
            
            _dataContext.Commit();
        }

        public IEnumerable<Currency> GetAllCurrencies()
        {
            var currencies = _dataContext
                .Currencies
                .ToArray();

            return currencies;
        }
    }
}