using System;
using System.Collections.Generic;
using ExchangeRates.Domain;

namespace ExchangeRates.Dal.Repositories
{
    public interface IExchangeRateRepository
    {
        IEnumerable<ExchangeRate> GetExchangeRatesOnDates(params DateTime[] dates);

        ExchangeRate GetExchangeRateForCurrencyActualOnDate(int currencyId, DateTime date);

        IEnumerable<Currency> GetAllCurrencies();

        void SaveRates(IEnumerable<ExchangeRate> exchangeRates);
    }
}