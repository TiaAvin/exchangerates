using Microsoft.EntityFrameworkCore;

namespace ExchangeRates.Core
{
    public interface IEntityMapping
    {
        void Map(ModelBuilder builder);
    }
}