import { Component } from '@angular/core';
import {Currency} from '../model/Currency';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  currencies: Currency[] = [
    { currencyId: 1, name: 'USD' },
    { currencyId: 2, name: 'JPY' }
  ];

  selectedCurrency: number;

  maxDateFilter = (date: Date): boolean => {
    const today = new Date();
    return date <= today;
  }
}
