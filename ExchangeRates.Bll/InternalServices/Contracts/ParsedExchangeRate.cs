using System;

namespace ExchangeRates.Bll.InternalServices.Contracts
{
    internal class ParsedExchangeRate
    {
        public DateTime Date { get; }

        public string TargetCurrency { get; }

        public int Amount { get; }
        
        public decimal ExchangeRate { get; }

        public ParsedExchangeRate(DateTime date, string targetCurrency, int amount, decimal exchangeRate)
        {
            Date = date;
            TargetCurrency = targetCurrency;
            Amount = amount;
            ExchangeRate = exchangeRate;
        }
    }
}