using System.Collections.Generic;
using ExchangeRates.Bll.InternalServices.Contracts;

namespace ExchangeRates.Bll.InternalServices.ExchangeRateUploading
{
    internal interface IExchangeRateUploadService
    {
        void UploadExchangeRates(IEnumerable<ParsedExchangeRate> parsedExchangeRates);
    }
}