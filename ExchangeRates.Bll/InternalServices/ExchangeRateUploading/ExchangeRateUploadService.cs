using System.Collections.Generic;
using System.Linq;
using ExchangeRates.Bll.InternalServices.Contracts;
using ExchangeRates.Dal.Repositories;
using ExchangeRates.Domain;

namespace ExchangeRates.Bll.InternalServices.ExchangeRateUploading
{
    internal class ExchangeRateUploadService : IExchangeRateUploadService
    {
        private readonly IExchangeRateRepository _exchangeRateRepository;
        
        public ExchangeRateUploadService(IExchangeRateRepository exchangeRateRepository)
        {
            _exchangeRateRepository = exchangeRateRepository;
        }

        public void UploadExchangeRates(IEnumerable<ParsedExchangeRate> parsedExchangeRates)
        {
            var rateDataCollection = (parsedExchangeRates ?? Enumerable.Empty<ParsedExchangeRate>()).ToArray();
            
            var existingCurrencies = _exchangeRateRepository.GetAllCurrencies().ToArray();
            
            var newCurrencies = GetNewCurrencies(rateDataCollection, existingCurrencies);

            var currencyDictionary = existingCurrencies.Union(newCurrencies).ToDictionary(x => x.Name);
            
            var dates = rateDataCollection.Select(x => x.Date.Date).ToArray();

            var existingRates = _exchangeRateRepository.GetExchangeRatesOnDates(dates)
                .Select(x => new
                {
                    CurrencyId = x.Target.Id,
                    Date = x.Date
                })
                .ToArray();

            var parsedRatesWithCurrencies = rateDataCollection
                .Select(x => new
                {
                    ExchangeRate = x,
                    Currency = currencyDictionary[x.TargetCurrency]
                })
                .Where(x => !existingRates.Any(e => e.Date == x.ExchangeRate.Date.Date && e.CurrencyId == x.Currency.Id))
                .Select(x => ConvertToExchangeRate(x.ExchangeRate, x.Currency))
                .ToArray();
            
            _exchangeRateRepository.SaveRates(parsedRatesWithCurrencies);
        }
        
        private ExchangeRate ConvertToExchangeRate(ParsedExchangeRate parsedExchangeRate, Currency currency)
        {
            return new ExchangeRate
            {
                Date = parsedExchangeRate.Date,
                Rate = parsedExchangeRate.ExchangeRate,
                Amount = parsedExchangeRate.Amount,
                Target = currency
            };
        }

        private Currency[] GetNewCurrencies(
            ParsedExchangeRate[] exchangeRateData, 
            Currency[] existingCurrencies)
        {
            var existingCurrencyNames = existingCurrencies
                .Select(x => x.Name)
                .ToHashSet();

            var newCurrencies = exchangeRateData
                .Select(x => x.TargetCurrency)
                .Distinct()
                .Where(x => !existingCurrencyNames.Contains(x))
                .Select(x => new Currency
                {
                    Name = x
                });

            return newCurrencies.ToArray();
        }
    }
}