namespace ExchangeRates.Bll.InternalServices.Validation
{
    internal static class ExchangeRateValidatorMessages
    {
        public const string CurrencyFullDataFormat = "Currency: {0}, date: {1}, amount: {2}, exchange rate: {3}";

        public const string CurrencyShortDataFormat = "Currency: {0}, Date {1}";
        
        public const string CurrencyAmountMustBeAPositiveNumber = "Currency amount must be a positive number";
        
        public const string RateMustBeAPositiveNumber = "Rate must be a positive number";
        
        public const string CurrencyNameHasIncorrectFormat = "Currency name has incorrect format";

        public const string DuplicatedRateDataFound = "Duplicated exchange rate data found";
    }
}