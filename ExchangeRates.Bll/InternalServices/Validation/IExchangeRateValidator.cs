using System.Collections.Generic;
using ExchangeRates.Bll.InternalServices.Contracts;

namespace ExchangeRates.Bll.InternalServices.Validation
{
    internal interface IExchangeRateValidator
    {
        void Validate(IEnumerable<ParsedExchangeRate> exchangeRatesData);
    }
}