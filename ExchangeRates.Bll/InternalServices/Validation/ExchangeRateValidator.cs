using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ExchangeRates.Bll.InternalServices.Contracts;
using ExchangeRates.Bll.InternalServices.Exceptions;

namespace ExchangeRates.Bll.InternalServices.Validation
{
    internal class ExchangeRateValidator : IExchangeRateValidator
    {
        private readonly Regex _currencyNameRegex = new Regex("[A-Z]{3}");
        
        public void Validate(IEnumerable<ParsedExchangeRate> exchangeRatesData)
        {
            var exchangeRates = exchangeRatesData?.ToArray() ?? new ParsedExchangeRate[0];
            
            if (!exchangeRates.Any())
            {
                return;
            }
            
            ValidateExchangeRateDataDoesntContainDuplicatedRecords(exchangeRates);

            foreach (var exchangeRate in exchangeRates)
            {
                ValidateCurrencyName(exchangeRate);
                ValidateAmount(exchangeRate);
                ValidateExchangeRateValue(exchangeRate);
            }
        }

        private void ValidateExchangeRateDataDoesntContainDuplicatedRecords(ParsedExchangeRate[] exchangeRates)
        {
            var duplicatedRecords = GetDuplicatedRecords(exchangeRates).ToArray();
            
            if (duplicatedRecords.Any())
            {
                throw new ExchangeRateValidationException(string.Format(
                    "{0}: {1}",
                    ExchangeRateValidatorMessages.DuplicatedRateDataFound,
                    string.Join(
                        ", ", 
                        duplicatedRecords.Select(x => string.Format(ExchangeRateValidatorMessages.CurrencyShortDataFormat, x.TargetCurrency, x.Date)))));
            }
        }

        private IEnumerable<ParsedExchangeRate> GetDuplicatedRecords(ParsedExchangeRate[] exchangeRates)
        {
            return exchangeRates
                .GroupBy(x => new { x.Date, x.TargetCurrency })
                .Where(x => x.Count() > 1)
                .SelectMany(x => x.ToArray());
        }

        private void ValidateAmount(ParsedExchangeRate rate)
        {
            if (rate.Amount <= 0)
            {
                throw new ExchangeRateValidationException($"{ExchangeRateValidatorMessages.CurrencyAmountMustBeAPositiveNumber}, ");
            }
        }

        private void ValidateCurrencyName(ParsedExchangeRate rate)
        {
            if (!_currencyNameRegex.IsMatch(rate.TargetCurrency))
            {
                throw new ExchangeRateValidationException(ExchangeRateValidatorMessages.CurrencyNameHasIncorrectFormat);
            }
        }

        private void ValidateExchangeRateValue(ParsedExchangeRate rate)
        {
            if (rate.ExchangeRate <= 0)
            {
                throw new ExchangeRateValidationException(ExchangeRateValidatorMessages.RateMustBeAPositiveNumber);
            }
        }
    }
}