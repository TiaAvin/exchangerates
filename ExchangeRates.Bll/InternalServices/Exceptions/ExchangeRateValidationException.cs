using System;

namespace ExchangeRates.Bll.InternalServices.Exceptions
{
    internal class ExchangeRateValidationException : Exception
    {
        private const string BaseMessage = "Exchange rates set validation error";
        
        public ExchangeRateValidationException(string message) : base($"{BaseMessage}: {message}")
        {
        }
    }
}