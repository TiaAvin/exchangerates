using System;

namespace ExchangeRates.Bll.InternalServices.Exceptions
{
    internal class ExchangeRatesParsingException : Exception
    {
        public ExchangeRatesParsingException(string message)
            : base(message)
        {
        }
    }
}