using System.Collections.Generic;
using System.IO;
using ExchangeRates.Bll.InternalServices.Contracts;

namespace ExchangeRates.Bll.InternalServices.ExchangeRateDataParsing
{
    internal interface IExchangeRatesParser
    {
        IEnumerable<ParsedExchangeRate> ParseFromStream(Stream dataStream);
    }
}