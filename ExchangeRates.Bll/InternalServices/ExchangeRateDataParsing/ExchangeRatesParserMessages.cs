namespace ExchangeRates.Bll.InternalServices.ExchangeRateDataParsing
{
    internal static class ExchangeRatesParserMessages
    {
        public const string DataStreamIsNull = "Data stream is null";

        public const string CantReadRatesDataFromStream = "Can't read rates data from stream";

        public const string CurrencyNameHasIncorrectFormat = "Currency name has incorrect format";

        public const string CurrencyAmountIsNotANumber = "Currency amount in not a number";

        public const string CurrencyAmountMustBeAPositiveNumber = "Currency amount must be a positive number";

        public const string CurrencyHeaderDataHasIncorrectFormat = "Currency header data has incorrect format";

        public const string HeaderDataNotFound = "Header data not found";

        public const string RateMustBeAFloatingPointNumber = "Rate must be a floating point number";
        
        public const string RateMustBeAPositiveNumber = "Rate must be a positive number";

        public const string DateHasIncorrectFormat = "Date has incorrect format, expected 'dd.mm.yyyy'";

        public const string ExchangeRatesDataCountDoesntMatchHeaderCount = "Exchange rates data count doesn't match header count";

        public const string ExchangeRatesRowIsEmpty = "Exchange rates row is empty";
    }
}