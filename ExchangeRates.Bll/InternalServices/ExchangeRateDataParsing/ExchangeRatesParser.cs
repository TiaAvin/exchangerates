using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ExchangeRates.Bll.InternalServices.Contracts;
using ExchangeRates.Bll.InternalServices.Exceptions;

namespace ExchangeRates.Bll.InternalServices.ExchangeRateDataParsing
{
    internal class ExchangeRatesParser : IExchangeRatesParser
    {
        private readonly Regex _currencyNameRegex = new Regex("[A-Z]{3}");
        
        public IEnumerable<ParsedExchangeRate> ParseFromStream(Stream dataStream)
        {
            ValidateDataStream(dataStream);

            dataStream.Position = 0;
            
            using (var reader = new StreamReader(dataStream))
            {
                var headerData = ParseHeader(reader);

                while (!reader.EndOfStream)
                {
                    var exchangeRatesRowString = reader.ReadLine();

                    if (string.IsNullOrWhiteSpace(exchangeRatesRowString))
                    {
                        throw new ExchangeRatesParsingException(ExchangeRatesParserMessages.ExchangeRatesRowIsEmpty);
                    }
                    
                    var exchangeRatesRow = exchangeRatesRowString.Trim().Split('|');
                    
                    var date = ParseDate(exchangeRatesRow.First());

                    var rates = ParseExchangeRates(exchangeRatesRow.Skip(1).ToArray(), headerData, date);

                    foreach (var rate in rates)
                    {
                        yield return rate;
                    }
                }
            }
        }

        private IEnumerable<ParsedExchangeRate> ParseExchangeRates(
            string[] exchangeRatesData, 
            Dictionary<int, CurrencyHeader> headerData,
            DateTime date)
        {
            if (exchangeRatesData.Count() != headerData.Count)
            {
                throw new ExchangeRatesParsingException(ExchangeRatesParserMessages.ExchangeRatesDataCountDoesntMatchHeaderCount);
            }

            var index = 0;

            foreach (var exchangeRateData in exchangeRatesData)
            {
                yield return new ParsedExchangeRate(date, headerData[index].Name, headerData[index].Amount, ParseRate(exchangeRateData));

                index++;
            }
        }

        private DateTime ParseDate(string dateString)
        {
            if (!DateTime.TryParseExact(dateString, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime date))
            {
                throw new ExchangeRatesParsingException(ExchangeRatesParserMessages.DateHasIncorrectFormat);
            }

            return date;
        }

        private decimal ParseRate(string rateString)
        {
            if (!decimal.TryParse(rateString, out decimal rate))
            {
                throw new ExchangeRatesParsingException(ExchangeRatesParserMessages.RateMustBeAFloatingPointNumber);
            }

            return rate;
        }

        private Dictionary<int, CurrencyHeader> ParseHeader(StreamReader reader)
        {
            var headerString = reader.ReadLine();

            if (string.IsNullOrWhiteSpace(headerString))
            {
                throw new ExchangeRatesParsingException(ExchangeRatesParserMessages.HeaderDataNotFound);
            }

            var index = 0;

            return headerString
                .Split('|')
                .Skip(1)
                .Select(x => new
                {
                    Index = index++,
                    CurrencyHeader = ParseCurrencyHeader(x)
                })
                .ToDictionary(x => x.Index, x => x.CurrencyHeader);
        }

        private CurrencyHeader ParseCurrencyHeader(string currencyHeaderString)
        {
            var currencyData = currencyHeaderString.Trim().Split(' ');

            if (currencyData.Length != 2)
            {
                throw new ExchangeRatesParsingException(ExchangeRatesParserMessages.CurrencyHeaderDataHasIncorrectFormat);
            }

            return new CurrencyHeader(
                ParseAmount(currencyData[0]), 
                currencyData[1]);
        }

        private int ParseAmount(string amountString)
        {
            if (!int.TryParse(amountString, out var amount))
            {
                throw new ExchangeRatesParsingException(ExchangeRatesParserMessages.CurrencyAmountIsNotANumber);
            }

            return amount;
        }

        private void ValidateDataStream(Stream stream)
        {
            if (stream == null || stream == Stream.Null)
            {
                throw new ExchangeRatesParsingException(ExchangeRatesParserMessages.DataStreamIsNull);
            }

            if (!stream.CanRead || !stream.CanSeek)
            {
                throw new ExchangeRatesParsingException(ExchangeRatesParserMessages.CantReadRatesDataFromStream);
            }
        }

        private class CurrencyHeader
        {
            public int Amount { get; }

            public string Name { get; }

            public CurrencyHeader(int amount, string name)
            {
                Amount = amount;
                Name = name;
            }
        }
    }
}