using System.IO;

namespace ExchangeRates.Bll.InternalServices.DataDownloadServices
{
    internal interface IWebClientProxy
    {
        Stream OpenRead(string uri);
    }
}