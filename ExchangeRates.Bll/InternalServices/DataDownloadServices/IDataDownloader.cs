using System.IO;

namespace ExchangeRates.Bll.InternalServices.DataDownloadServices
{
    internal interface IDataDownloader
    {
        
        Stream DownloadOnYear(int year);
    }
}