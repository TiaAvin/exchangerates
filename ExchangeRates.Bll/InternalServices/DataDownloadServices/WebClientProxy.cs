using System.IO;
using System.Net;

namespace ExchangeRates.Bll.InternalServices.DataDownloadServices
{
    internal class WebClientProxy : IWebClientProxy
    {
        public Stream OpenRead(string uri)
        {
            using (var webClient = new WebClient())
            {
                return webClient.OpenRead(uri);
            }
        }
    }
}