using System.IO;

namespace ExchangeRates.Bll.InternalServices.DataDownloadServices
{
    internal class DataDownloader : IDataDownloader
    {
        private readonly IWebClientProxy _webClientProxy;

        public DataDownloader(IWebClientProxy webClientProxy)
        {
            _webClientProxy = webClientProxy;
        }

        public Stream DownloadOnYear(int year)
        {
            var requestString = string.Format(Constants.ExchangeRatesDataUri, year);

            return _webClientProxy.OpenRead(requestString);
        }
    }
}