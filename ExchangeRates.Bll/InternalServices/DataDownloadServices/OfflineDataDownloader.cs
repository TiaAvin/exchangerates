using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace ExchangeRates.Bll.InternalServices.DataDownloadServices
{
    internal class OfflineDataDownloader : IDataDownloader
    {
        public Stream DownloadOnYear(int year)
        {
            var assemblyDirectoryName = Directory.GetCurrentDirectory().Split("\\").Last();

            var formattedAssemblyDirectoryName = Regex.Replace(assemblyDirectoryName, @"\.", @"\.");

            var assemblyNamePattern = $"{@"\\"}{formattedAssemblyDirectoryName}$";
            
            var directory = Regex.Replace(Directory.GetCurrentDirectory(), assemblyNamePattern, string.Empty);

            var dataFilePath = Path.Combine(directory, "Data");

            var dataFileFullName = Path.Combine(dataFilePath, "Rates.txt");

            if (!File.Exists(dataFileFullName))
            {
                throw new FileNotFoundException($"Data file {dataFileFullName} not found");
            }

            return new FileStream(dataFileFullName, FileMode.Open, FileAccess.Read, FileShare.Read);
        }
    }
}