using Autofac;
using ExchangeRates.Bll.ExchangeRateServices;
using ExchangeRates.Bll.InternalServices.DataDownloadServices;
using ExchangeRates.Bll.InternalServices.ExchangeRateDataParsing;
using ExchangeRates.Bll.InternalServices.ExchangeRateUploading;
using ExchangeRates.Bll.InternalServices.Validation;
using ExchangeRates.Core;

namespace ExchangeRates.Bll
{
    public class BllModule : Module
    {
        private readonly bool _offlineMode;

        public BllModule(bool offlineMode)
        {
            _offlineMode = offlineMode;
        }

        protected override void Load(ContainerBuilder builder)
        {
            if (_offlineMode)
            {
                builder.RegisterType<OfflineDataDownloader>().As<IDataDownloader>().SingleInstance();
            }
            else
            {
                builder.RegisterType<DataDownloader>().As<IDataDownloader>().SingleInstance();
            }

            builder.RegisterType<WebClientProxy>().As<IWebClientProxy>().SingleInstance();

            builder.RegisterType<ExchangeRatesParser>().As<IExchangeRatesParser>().SingleInstance();

            builder.RegisterType<ExchangeRateUploadService>().As<IExchangeRateUploadService>().SingleInstance();

            builder.RegisterType<ExchangeRateValidator>().As<IExchangeRateValidator>().SingleInstance();

            builder.RegisterType<ExchangeRateDownloadService>().As<IExchangeRateDownloadService>().SingleInstance();

            builder.RegisterType<ExchangeRateDataRetrievalService>().As<IExchangeRateDataRetrievalService>()
                .SingleInstance();
        }
    }
}