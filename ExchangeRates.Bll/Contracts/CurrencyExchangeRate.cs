namespace ExchangeRates.Bll.Contracts
{
    public class CurrencyExchangeRate
    {
        public string CurrencyName { get; }

        public decimal ExchangeRate { get; }

        public CurrencyExchangeRate(string currencyName, decimal exchangeRate)
        {
            CurrencyName = currencyName;
            ExchangeRate = exchangeRate;
        }
    }
}