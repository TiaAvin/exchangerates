namespace ExchangeRates.Bll.Contracts
{
    public class DownloadExchangeRatesResult
    {
        public bool HasError { get; }

        public string ErrorMessage { get; }

        private DownloadExchangeRatesResult(bool hasError, string errorMessage)
        {
            HasError = hasError;
            ErrorMessage = errorMessage;
        }

        public static DownloadExchangeRatesResult Success()
        {
            return new DownloadExchangeRatesResult(false, null);
        }

        public static DownloadExchangeRatesResult Error(string message)
        {
            return new DownloadExchangeRatesResult(true, message ?? string.Empty);
        }
    }
}