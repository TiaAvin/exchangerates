using System;
using System.Collections.Generic;
using ExchangeRates.Bll.Contracts;
using ExchangeRates.Domain;

namespace ExchangeRates.Bll.ExchangeRateServices
{
    public interface IExchangeRateDataRetrievalService
    {
        IEnumerable<Currency> GetCurrencies();

        CurrencyExchangeRate GetExchangeRateForCurrencyToKronaActualOnDate(int currencyId, DateTime date);
    }
}