using ExchangeRates.Bll.Contracts;

namespace ExchangeRates.Bll.ExchangeRateServices
{
    public interface IExchangeRateDownloadService
    {
        DownloadExchangeRatesResult DownloadExchangeRatesOnYear(int year);
    }
}