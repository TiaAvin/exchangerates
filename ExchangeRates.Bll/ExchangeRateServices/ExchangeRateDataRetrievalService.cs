using System;
using System.Collections.Generic;
using ExchangeRates.Bll.Contracts;
using ExchangeRates.Dal.Repositories;
using ExchangeRates.Domain;

namespace ExchangeRates.Bll.ExchangeRateServices
{
    internal class ExchangeRateDataRetrievalService : IExchangeRateDataRetrievalService
    {
        private readonly IExchangeRateRepository _exchangeRateRepository;
        
        public ExchangeRateDataRetrievalService(IExchangeRateRepository exchangeRateRepository)
        {
            _exchangeRateRepository = exchangeRateRepository;
        }

        public IEnumerable<Currency> GetCurrencies()
        {
            return _exchangeRateRepository.GetAllCurrencies();
        }
        
        public CurrencyExchangeRate GetExchangeRateForCurrencyToKronaActualOnDate(int currencyId, DateTime date)
        {
            var kronaExchangeRate =
                _exchangeRateRepository.GetExchangeRateForCurrencyActualOnDate(currencyId, date.Date);

            if (kronaExchangeRate == null)
            {
                return null;
            }

            return new CurrencyExchangeRate(kronaExchangeRate.Target.Name, 1 / kronaExchangeRate.Rate);
        }
    }
}