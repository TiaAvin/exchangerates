using System;
using System.Linq;
using ExchangeRates.Bll.Contracts;
using ExchangeRates.Bll.InternalServices.DataDownloadServices;
using ExchangeRates.Bll.InternalServices.ExchangeRateDataParsing;
using ExchangeRates.Bll.InternalServices.ExchangeRateUploading;
using ExchangeRates.Bll.InternalServices.Validation;

namespace ExchangeRates.Bll.ExchangeRateServices
{
    internal class ExchangeRateDownloadService : IExchangeRateDownloadService
    {
        private readonly IDataDownloader _dataDownloader;

        private readonly IExchangeRatesParser _exchangeRatesParser;

        private readonly IExchangeRateValidator _exchangeRateValidator;

        private readonly IExchangeRateUploadService _exchangeRateUploadService;
        
        public ExchangeRateDownloadService(
            IDataDownloader dataDownloader,
            IExchangeRatesParser exchangeRatesParser,
            IExchangeRateValidator exchangeRateValidator,
            IExchangeRateUploadService exchangeRateUploadService)
        {
            _dataDownloader = dataDownloader;
            _exchangeRatesParser = exchangeRatesParser;
            _exchangeRateValidator = exchangeRateValidator;
            _exchangeRateUploadService = exchangeRateUploadService;
        }

        public DownloadExchangeRatesResult DownloadExchangeRatesOnYear(int year)
        {
            try
            {
                using (var dataStream = _dataDownloader.DownloadOnYear(year))
                {
                    var parsedRates = _exchangeRatesParser.ParseFromStream(dataStream).ToArray();
                    
                    _exchangeRateValidator.Validate(parsedRates);
                
                    _exchangeRateUploadService.UploadExchangeRates(parsedRates);
                }
                
                return DownloadExchangeRatesResult.Success();
            }
            catch (Exception e)
            {
                return DownloadExchangeRatesResult.Error(e.Message);
            }
        }
    }
}