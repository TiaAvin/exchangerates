namespace ExchangeRates.Bll
{
    internal static class Constants
    {
        public const string ExchangeRatesDataUri = "https://www.cnb.cz/en/financial_markets/foreign_exchange_market/exchange_rate_fixing/year.txt?year={0}";
    }
}