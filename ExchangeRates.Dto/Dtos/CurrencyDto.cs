using System.Runtime.Serialization;

namespace ExchangeRates.Dto.Dtos
{
    [DataContract]
    public class CurrencyDto
    {
        [DataMember]
        public int CurrencyId { get; private set; }

        [DataMember]
        public string Name { get; private set; }
    }
}