using System;

namespace ExchangeRates.Dto.Dtos
{
    public class ExchangeRateDto
    {
        public string CurrencyName { get; private set; }

        public decimal ExchangeRate { get; private set; }
    }
}