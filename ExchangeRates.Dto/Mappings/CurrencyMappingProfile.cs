using AutoMapper;
using ExchangeRates.Domain;
using ExchangeRates.Dto.Dtos;

namespace ExchangeRates.Dto.Mappings
{
    public class CurrencyMappingProfile : Profile
    {
        public CurrencyMappingProfile()
        {
            CreateMap<Currency, CurrencyDto>()
                .ForMember(x => x.CurrencyId, s => s.MapFrom(c => c.Id))
                .ForMember(x => x.Name, s => s.MapFrom(c => c.Name));
        }
    }
}