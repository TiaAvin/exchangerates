using AutoMapper;
using ExchangeRates.Bll.Contracts;
using ExchangeRates.Dto.Dtos;

namespace ExchangeRates.Dto.Mappings
{
    public class ExchangeRateMappingProfile : Profile
    {
        public ExchangeRateMappingProfile()
        {
            CreateMap<CurrencyExchangeRate, ExchangeRateDto>()
                .ForMember(x => x.CurrencyName, s => s.MapFrom(c => c.CurrencyName))
                .ForMember(x => x.ExchangeRate, s => s.MapFrom(c => c.ExchangeRate));
        }
    }
}