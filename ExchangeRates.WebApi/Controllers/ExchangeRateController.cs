﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ExchangeRates.Bll.ExchangeRateServices;
using ExchangeRates.Dto.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ExchangeRates.WebApi.Controllers
{
    [ApiController]
    public class ExchangeRateController : ControllerBase
    {
        private readonly IExchangeRateDataRetrievalService _exchangeRateDataRetrievalService;

        private readonly IMapper _mapper;
        
        public ExchangeRateController(
            IExchangeRateDataRetrievalService exchangeRateDataRetrievalService,
            IMapper mapper)
        {
            _exchangeRateDataRetrievalService = exchangeRateDataRetrievalService;
            _mapper = mapper;
        }

        [Route("currencies")]
        [HttpGet]
        public ActionResult<IEnumerable<CurrencyDto>> Get()
        {
            try
            {
                var rateDtos = _exchangeRateDataRetrievalService.GetCurrencies()
                    .Select(_mapper.Map<CurrencyDto>)
                    .ToArray();

                return new ActionResult<IEnumerable<CurrencyDto>>(rateDtos);
            }
            catch (Exception e)
            {
                return CreateErrorResult(e.Message);
            }
        }
        
        [Route("exchange-rate/{date}/{currencyId}")]
        [HttpGet]
        public ActionResult<ExchangeRateDto> Get(DateTime date, int currencyId)
        {
            try
            {
                var exchangeRate = _exchangeRateDataRetrievalService
                    .GetExchangeRateForCurrencyToKronaActualOnDate(currencyId, date);

                if (exchangeRate == null)
                {
                    return new JsonResult($"Requested exchange rate not found") { StatusCode = StatusCodes.Status204NoContent };
                }
                
                return new ActionResult<ExchangeRateDto>(_mapper.Map<ExchangeRateDto>(exchangeRate));
            }
            catch (Exception e)
            {
                return CreateErrorResult(e.Message);
            }
        }

        private ActionResult CreateErrorResult(string message)
        {
            return new JsonResult(message) { StatusCode = StatusCodes.Status500InternalServerError };
        }
    }
}