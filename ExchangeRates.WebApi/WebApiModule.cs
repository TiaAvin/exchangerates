using Autofac;
using ExchangeRates.WebApi.Controllers;

namespace ExchangeRates.WebApi
{
    public class WebApiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ExchangeRateController>().InstancePerRequest();
        }
    }
}