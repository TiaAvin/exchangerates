﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using ExchangeRates.Bll;
using ExchangeRates.Dal;
using ExchangeRates.Data;
using ExchangeRates.DbMappings;
using ExchangeRates.DbMappings.Mappings;
using ExchangeRates.Dto.Mappings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ExchangeRates.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var autoMapper = ConfigureAutoMapper();

            services.AddSingleton(autoMapper);

            var container = ConfigureDiContainer(services);
            
            return new AutofacServiceProvider(container);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }

        public IContainer ConfigureDiContainer(IServiceCollection services)
        {
            var builder = new ContainerBuilder();
            
            builder.Populate(services);
            
            builder.RegisterModule<DbMappingsModule>();
            builder.RegisterModule<DataModule>();
            builder.RegisterModule<DalModule>();
            builder.RegisterModule(new BllModule(true));
            builder.RegisterModule<WebApiModule>();

            return builder.Build();
        }

        private IMapper ConfigureAutoMapper()
        {
            var configuration = new MapperConfiguration(x =>
            {
                x.AddProfile<CurrencyMappingProfile>();
                x.AddProfile<ExchangeRateMappingProfile>();
            });

            return configuration.CreateMapper();
        }
    }
}