using System;
using ExchangeRates.Bll.InternalServices.Contracts;
using ExchangeRates.Bll.InternalServices.Exceptions;
using ExchangeRates.Bll.InternalServices.Validation;
using NUnit.Framework;

namespace ExchangeRates.Bll.Tests.ExchangeRateValidatorTests
{
    [TestFixture]
    public class Validate
    {
        private ExchangeRateValidator _exchangeRateValidator;
        
        [OneTimeSetUp]
        public void FixtureSetUp()
        {
            _exchangeRateValidator = new ExchangeRateValidator();
        }
        
        [Test]
        public void WhenCurrencyAmountIsZeroThrowsException()
        {
            var parsedRate = new ParsedExchangeRate(new DateTime(2018, 07, 07), "JPY", 0, 1);
            
            Assert.That(
                () => _exchangeRateValidator.Validate(new[] { parsedRate }),
                Throws.Exception.TypeOf<ExchangeRateValidationException>()
                    .With.Message.Contains(ExchangeRateValidatorMessages.CurrencyAmountMustBeAPositiveNumber));
        }
        
        [Test]
        public void WhenCurrencyAmountIsNegativeThrowsException()
        {
            var parsedRate = new ParsedExchangeRate(new DateTime(2018, 07, 07), "JPY", -1, 1);

            Assert.That(
                () => _exchangeRateValidator.Validate(new[] { parsedRate }),
                Throws.Exception.TypeOf<ExchangeRateValidationException>()
                    .With.Message.Contains(ExchangeRateValidatorMessages.CurrencyAmountMustBeAPositiveNumber));
        }
        
        [Test]
        public void WhenCurrencyNameContainsDigitsThrowsException()
        {
            var parsedRate = new ParsedExchangeRate(new DateTime(2018, 07, 07), "8PY", 10, 1);

            Assert.That(
                () => _exchangeRateValidator.Validate(new[] { parsedRate }),
                Throws.Exception.TypeOf<ExchangeRateValidationException>()
                    .With.Message.Contains(ExchangeRateValidatorMessages.CurrencyNameHasIncorrectFormat));
        }
        
        [Test]
        public void WhenCurrencyNameContainsSpecialCharactersThrowsException()
        {
            var parsedRate = new ParsedExchangeRate(new DateTime(2018, 07, 07), "&PY", 10, 1);

            Assert.That(
                () => _exchangeRateValidator.Validate(new[] { parsedRate }),
                Throws.Exception.TypeOf<ExchangeRateValidationException>()
                    .With.Message.Contains(ExchangeRateValidatorMessages.CurrencyNameHasIncorrectFormat));
        }
        
        [Test]
        public void WhenCurrencyNameContainsNotLatinLettersThrowsException()
        {
            var parsedRate = new ParsedExchangeRate(new DateTime(2018, 07, 07), "ЩPY", 10, 1);

            Assert.That(
                () => _exchangeRateValidator.Validate(new[] { parsedRate }),
                Throws.Exception.TypeOf<ExchangeRateValidationException>()
                    .With.Message.Contains(ExchangeRateValidatorMessages.CurrencyNameHasIncorrectFormat));
        }
        
        [Test]
        public void WhenRateValueIsZeroThrowsException()
        {
            var parsedRate = new ParsedExchangeRate(new DateTime(2018, 07, 07), "JPY", 10, 0);
            
            Assert.That(
                () => _exchangeRateValidator.Validate(new[] { parsedRate }),
                Throws.Exception.TypeOf<ExchangeRateValidationException>()
                    .With.Message.Contains(ExchangeRateValidatorMessages.RateMustBeAPositiveNumber));
        }
        
        [Test]
        public void WhenRateValueIsNegativeThrowsException()
        {
            var parsedRate = new ParsedExchangeRate(new DateTime(2018, 07, 07), "JPY", 10, -1);
            
            Assert.That(
                () => _exchangeRateValidator.Validate(new[] { parsedRate }),
                Throws.Exception.TypeOf<ExchangeRateValidationException>()
                    .With.Message.Contains(ExchangeRateValidatorMessages.RateMustBeAPositiveNumber));
        }
    }
}