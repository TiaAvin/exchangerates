using System;
using System.IO;
using ExchangeRates.Bll.InternalServices.DataDownloadServices;
using Moq;
using NUnit.Framework;

namespace ExchangeRates.Bll.Tests.DataDownloaderTests
{
    [TestFixture]
    public class DownloadOnDate
    {
        private DataDownloader _dataDownloader;

        private Mock<IWebClientProxy> _webClientProxyMock;

        private Stream _testStream;

        [SetUp]
        public void SetUp()
        {
            _testStream = new MemoryStream();
            
            _webClientProxyMock = new Mock<IWebClientProxy>();

            _webClientProxyMock
                .Setup(x => x.OpenRead(It.IsAny<string>()))
                .Returns(_testStream);
            
            _dataDownloader = new DataDownloader(_webClientProxyMock.Object);
        }

        [TearDown]
        public void TearDown()
        {
            _testStream.Dispose();
        }
        
        [Test]
        public void WebClientProxyOpenReadWasCalled()
        {
            _dataDownloader.DownloadOnYear(2015);
            
            _webClientProxyMock.Verify(x => x.OpenRead(It.IsAny<string>()), Times.Once);
        }
        
        [Test]
        public void CorrectUriWasPassedToWebClientProxy()
        {
            _dataDownloader.DownloadOnYear(2015);

            _webClientProxyMock.Verify(x => x.OpenRead(It.Is<string>(
                s => string.Equals(s, string.Format(Constants.ExchangeRatesDataUri, 2015)))));
        }
    }
}