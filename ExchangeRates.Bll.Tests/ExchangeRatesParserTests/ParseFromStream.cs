using System;
using System.IO;
using System.Linq;
using ExchangeRates.Bll.InternalServices.Exceptions;
using ExchangeRates.Bll.InternalServices.ExchangeRateDataParsing;
using NUnit.Framework;

namespace ExchangeRates.Bll.Tests.ExchangeRatesParserTests
{
    [TestFixture]
    public class ParseFromStream
    {
        private ExchangeRatesParser _exchangeRatesParser;

        private Stream _testDataStream;

        private StreamWriter _testDataWriter;
        
        [OneTimeSetUp]
        public void FixtureSetUp()
        {
            _exchangeRatesParser = new ExchangeRatesParser();
        }

        [SetUp]
        public void TestSetUp()
        {
            _testDataStream = new MemoryStream();
            _testDataWriter = new StreamWriter(_testDataStream) { AutoFlush = true };
        }

        [TearDown]
        public void TestTearDown()
        {
            _testDataWriter.Dispose();
            _testDataStream.Dispose();
        }

        [Test]
        public void WhenStreamInstanceIsNullThrowsException()
        {
            Assert.That(
                () => _exchangeRatesParser.ParseFromStream(null).ToArray(),
                Throws.Exception.TypeOf<ExchangeRatesParsingException>()
                    .With.Message.EqualTo(ExchangeRatesParserMessages.DataStreamIsNull));
        }
        
        [Test]
        public void WhenStreamIsStreamNullThrowsException()
        {
            Assert.That(
                () => _exchangeRatesParser.ParseFromStream(Stream.Null).ToArray(),
                Throws.Exception.TypeOf<ExchangeRatesParsingException>()
                    .With.Message.EqualTo(ExchangeRatesParserMessages.DataStreamIsNull));
        }
        
        [Test]
        public void WhenStreamContainsEmptyLineThrowsException()
        {
            _testDataWriter.WriteLine(string.Empty);
            
            Assert.That(
                () => _exchangeRatesParser.ParseFromStream(_testDataStream).ToArray(),
                Throws.Exception.TypeOf<ExchangeRatesParsingException>()
                    .With.Message.EqualTo(ExchangeRatesParserMessages.HeaderDataNotFound));
        }

        // Rider has some weird issue with running tests from test cases((
//        [TestCase("Date|0AUD", ExchangeRatesParserMessages.CurrencyHeaderDataHasIncorrectFormat, TestName = "WhenHeaderHasIncorrectFormatThrowsException")]
//        [TestCase("Date|0 AUD", ExchangeRatesParserMessages.CurrencyAmountMustBeAPositiveNumber, TestName = "WhenCurrencyAmountIsZeroThrowsException")]
//        [TestCase("Date|-1 AUD", ExchangeRatesParserMessages.CurrencyAmountMustBeAPositiveNumber, TestName = "WhenCurrencyAmountIsNegativeThrowsException")]
//        [TestCase("Date|1 A1D", ExchangeRatesParserMessages.CurrencyNameHasIncorrectFormat, TestName = "WhenCurrencyNameContainsDigitsThrowsException")]
//        [TestCase("Date|1 A%D", ExchangeRatesParserMessages.CurrencyNameHasIncorrectFormat, TestName = "WhenCurrencyNameContainsSpecialCharactersThrowsException")]
//        [TestCase("Date|1 AИD", ExchangeRatesParserMessages.CurrencyNameHasIncorrectFormat, TestName = "WhenCurrencyNameContainsNotLatinLettersThrowsException")]
//        public void WhenHeaderDataIsIncorrect(string headerData, string errorMessage)
//        {
//            _testDataWriter.WriteLine(headerData);
//
//            Assert.That(
//                () => _exchangeRatesParser.ParseFromStream(_testDataStream).ToArray(),
//                Throws.Exception.TypeOf<ExchangeRatesParsingException>().With.Message.EqualTo(errorMessage));
//        }

        [Test]
        public void WhenHeaderHasIncorrectFormatThrowsException()
        {
            _testDataWriter.WriteLine("Date|0AUD");

            Assert.That(
                () => _exchangeRatesParser.ParseFromStream(_testDataStream).ToArray(),
                Throws.Exception.TypeOf<ExchangeRatesParsingException>()
                    .With.Message.EqualTo(ExchangeRatesParserMessages.CurrencyHeaderDataHasIncorrectFormat));
        }
        
        [Test]
        public void WhenStreamContainsOnlyHeaderDoesntThrowsException()
        {
            _testDataWriter.WriteLine("Date|1 AUD");
            
            Assert.DoesNotThrow(() => _exchangeRatesParser.ParseFromStream(_testDataStream).ToArray());
        }
        
        [Test]
        public void WhenStreamContainsOnlyHeaderReturnsEmptyResult()
        {
            _testDataWriter.WriteLine("Date|1 AUD");

            var result = _exchangeRatesParser.ParseFromStream(_testDataStream).ToArray();
            
            Assert.That(result, Is.Empty);
        }
        
        [Test]
        public void WhenRateDateHasIncorrectFormatThrowsException()
        {
            _testDataWriter.WriteLine("Date|1 AUD");
            _testDataWriter.WriteLine("25-07-2015|18.665");
            
            Assert.That(
                () => _exchangeRatesParser.ParseFromStream(_testDataStream).ToArray(),
                Throws.Exception.TypeOf<ExchangeRatesParsingException>()
                    .With.Message.EqualTo(ExchangeRatesParserMessages.DateHasIncorrectFormat));
        }
        
        [Test]
        public void WhenRateValueHasIncorrectFormatThrowsException()
        {
            _testDataWriter.WriteLine("Date|1 AUD");
            _testDataWriter.WriteLine("25.07.2015|aaa");
            
            Assert.That(
                () => _exchangeRatesParser.ParseFromStream(_testDataStream).ToArray(),
                Throws.Exception.TypeOf<ExchangeRatesParsingException>()
                    .With.Message.EqualTo(ExchangeRatesParserMessages.RateMustBeAFloatingPointNumber));
        }
        
        [Test]
        public void WhenRateValueIsCorrectReturnsParsedRateDataForEachRow()
        {
            _testDataWriter.WriteLine("Date|1 AUD");
            _testDataWriter.WriteLine("25.07.2015|1.85");
            _testDataWriter.WriteLine("26.07.2015|1.87");

            var result = _exchangeRatesParser.ParseFromStream(_testDataStream).ToArray();
            
            Assert.That(result.Length, Is.EqualTo(2));
        }
        
        [Test]
        public void WhenRateValueIsCorrectReturnsCorrectDataInParsedRate()
        {
            _testDataWriter.WriteLine("Date|2 AUD");
            _testDataWriter.WriteLine("25.07.2015|1.85");

            var result = _exchangeRatesParser.ParseFromStream(_testDataStream).First();
            
            Assert.That(result.Date, Is.EqualTo(new DateTime(2015, 07, 25)));
            Assert.That(result.ExchangeRate, Is.EqualTo(1.85));
            Assert.That(result.TargetCurrency, Is.EqualTo("AUD"));
            Assert.That(result.Amount, Is.EqualTo(2));
        }
    }
}