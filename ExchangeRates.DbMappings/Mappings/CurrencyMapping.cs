using ExchangeRates.Core;
using ExchangeRates.Domain;
using Microsoft.EntityFrameworkCore;

namespace ExchangeRates.DbMappings.Mappings
{
    public class CurrencyMapping : IEntityMapping
    {
        public void Map(ModelBuilder builder)
        {
            builder.Entity<Currency>()
                .HasKey(x => x.Id);

            builder.Entity<Currency>()
                .Property(x => x.Name)
                .IsUnicode()
                .HasMaxLength(15)
                .IsRequired();

            builder.Entity<Currency>()
                .HasIndex(x => x.Name)
                .IsUnique();
        }
    }
}