using ExchangeRates.Core;
using ExchangeRates.Domain;
using Microsoft.EntityFrameworkCore;

namespace ExchangeRates.DbMappings.Mappings
{
    public class ExchangeRateMapping : IEntityMapping
    {
        public void Map(ModelBuilder builder)
        {
            builder.Entity<ExchangeRate>()
                .HasKey(x => x.Id);

            builder.Entity<ExchangeRate>()
                .HasOne(x => x.Target)
                .WithMany()
                .HasForeignKey("TargetId")
                .IsRequired();

            builder.Entity<ExchangeRate>()
                .Property(x => x.Date)
                .IsRequired();

            builder.Entity<ExchangeRate>()
                .Property(x => x.Rate)
                .IsRequired();
        }
    }
}