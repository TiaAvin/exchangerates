using Autofac;
using ExchangeRates.Core;
using ExchangeRates.DbMappings.Mappings;

namespace ExchangeRates.DbMappings
{
    public class DbMappingsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CurrencyMapping>().As<IEntityMapping>().SingleInstance();
            
            builder.RegisterType<ExchangeRateMapping>().As<IEntityMapping>().SingleInstance();
        }
    }
}