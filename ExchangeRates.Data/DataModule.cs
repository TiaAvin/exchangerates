using Autofac;
using ExchangeRates.Core;

namespace ExchangeRates.Data
{
    public class DataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DataContext>().As<IDataContext>().SingleInstance();
        }
    }
}