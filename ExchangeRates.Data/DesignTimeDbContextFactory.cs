using System;
using System.Linq;
using ExchangeRates.Core;
using Microsoft.EntityFrameworkCore.Design;

namespace ExchangeRates.Data
{
    internal class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var mappings = AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => typeof(IEntityMapping).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
                .Select(Activator.CreateInstance)
                .Cast<IEntityMapping>();
            
            return new DataContext(mappings);
        }
    }
}