using ExchangeRates.Domain;
using Microsoft.EntityFrameworkCore;

namespace ExchangeRates.Data
{
    public interface IDataContext
    {
        DbSet<Currency> Currencies { get; }
        
        DbSet<ExchangeRate> ExchangeRates { get; }

        void Commit();
    }
}