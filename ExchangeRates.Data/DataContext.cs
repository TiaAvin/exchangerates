using System.Collections.Generic;
using System.Linq;
using ExchangeRates.Core;
using ExchangeRates.Domain;
using Microsoft.EntityFrameworkCore;

namespace ExchangeRates.Data
{
    internal class DataContext : DbContext, IDataContext
    {
        private readonly IEnumerable<IEntityMapping> _entityMappings;
        
        public DbSet<Currency> Currencies { get; set; }

        public DbSet<ExchangeRate> ExchangeRates { get; set; }
        public void Commit()
        {
            SaveChanges();
        }

        public DataContext(IEnumerable<IEntityMapping> entityMappings)
        {
            _entityMappings = entityMappings ?? Enumerable.Empty<IEntityMapping>();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=ExchangeRates;Username=postgres;Password=postgres");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var entityMapping in _entityMappings)
            {
                entityMapping.Map(modelBuilder);
            }
        }
    }
}