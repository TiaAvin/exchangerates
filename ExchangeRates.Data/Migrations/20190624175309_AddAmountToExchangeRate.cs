﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ExchangeRates.Data.Migrations
{
    public partial class AddAmountToExchangeRate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Amount",
                table: "ExchangeRates",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount",
                table: "ExchangeRates");
        }
    }
}
