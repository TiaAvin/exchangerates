﻿using System;
using Autofac;
using ExchangeRates.Bll;
using ExchangeRates.Bll.ExchangeRateServices;
using ExchangeRates.Dal;
using ExchangeRates.Data;
using ExchangeRates.DbMappings;

namespace ExchangeRates.RateDataUploadConsole
{
    static class DataUploadConsole
    {
        public static void Main(string[] args)
        {
            var container = ConfigureDiContainer();
            
            ReadCommand(container);
        }

        private static void ReadCommand(IContainer container)
        {
            Console.WriteLine("Select an action:" + Environment.NewLine +
                                     "1: Download exchange rate data" + Environment.NewLine +
                                     "2: Exit" + Environment.NewLine);

            var command = string.Empty;

            while (command != "2")
            {
                Console.Write("Selected action: ");
                command = Console.ReadLine();

                if (command == "1")
                {
                    ExecuteDownloadExchangeRateDataCommand(container);
                }
            }
        }

        private static IContainer ConfigureDiContainer()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule<DbMappingsModule>();
            containerBuilder.RegisterModule<DataModule>();
            containerBuilder.RegisterModule<DalModule>();
            containerBuilder.RegisterModule(new BllModule(true));

            return containerBuilder.Build();
        }

        private static void ExecuteDownloadExchangeRateDataCommand(IContainer container)
        {
            Console.Write($"Enter year: ");

            var yearString = Console.ReadLine();

            if (int.TryParse(yearString, out int year))
            {
                var dataDownloader = container.Resolve<IExchangeRateDownloadService>();

                var result = dataDownloader.DownloadExchangeRatesOnYear(year);

                Console.WriteLine(!result.HasError
                    ? "Exchange rates were updated successfully"
                    : $"Error occured while updating exchange rates: {result.ErrorMessage}");
            }
        }
    }
}